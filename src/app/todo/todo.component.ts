import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {TodosService } from '../todos.service';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  @Output() myButtonClicked=new EventEmitter<any>();
  
  text;
  status:boolean;

  key;
  showEditField = false;
  showTheButton = true;
  tempText;

  
  
  toDelete(){
    this.todosService.deleteTodo(this.key);
  }

 
  save(){
    this.todosService.update(this.key, this.text,this.status);
    this.showEditField = false;
  }
  showEdit(){
    this.showEditField = true;
    this.tempText = this.text;
    this.showTheButton = false;
  }
  cancel(){
    this.showEditField = false;
    this.text = this.tempText;
  }
  checkChange()
  {
    this.todosService.updateDone(this.key,this.text,this.status);
  }



  constructor(private todosService:TodosService
              ) { }

  ngOnInit() {
    this.text = this.data.text;
    this.status=this.data.done;
    this.key = this.data.$key;
  
  }
}
