import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { TodosService } from '../todos.service';


@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  todoTextFromTodo='No todos task so far';
  todos = [ ];
  text:string;
  status:boolean;
  key;
  taskStatus = [];
 

  showTodo()
  {
    this.authService.user.subscribe(user => {
      this.db.list('/users/' + user.uid + '/todos').snapshotChanges().subscribe(
        todos => {
          this.todos = [];
          todos.forEach(
            todo => {
              let y = todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
            }
          )
        }  
      )
    })
  }


  toFilter()
  {
    this.authService.user.subscribe(user => {
    this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
      todos =>{
        this.todos = [];
        todos.forEach(
          todo => {
            let y = todo.payload.toJSON();
            y["$key"] = todo.key;          
              if (y['done'] == this.status) {
                this.todos.push(y);
              }
              else if (this.status != true && this.status != false)
              {
                this.todos.push(y);
                console.log("todos"+this.todos);
              }
              
          }
        )
      }
    )
    })
  }
 
  
 
  addTodo(){
    this.todosService.addTodo(this.text, this.status);
    this.text = '';
    this.status=false;
    
  }

  
  constructor(private router:Router,
              private authService:AuthService,
              private db:AngularFireDatabase,
              private todosService:TodosService,
             ) 
              { 
                this.showTodo();
            }
          
  ngOnInit() {
    console.log(this.todos.length)
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{
          this.todos = [];
          this.taskStatus = [];
          todos.forEach(
            todo => {
              let y =todo.payload.toJSON();
              y["$key"] = todo.key;
              this.todos.push(y);
              let sta = y['done'];
              if (this.taskStatus.indexOf(sta)== -1)
               {
               this.taskStatus.push(sta);
              }
             
            }
          )
        }
      ) 
    })
  }

}
