import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from 'rxjs';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class TodosService {

  

  addTodo(text:string,done:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').push({'text':text, 'done':false});
    })
  }
  
  update(key:string, text:string, done:boolean){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text,'done':done});
    })
  }

  deleteTodo(key:string){
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').remove(key);
    })
  }

  updateDone(key:string, text:string, done:boolean)
  {
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/todos').update(key,{'text':text, 'done':done});
    })
    
  }
  user:Observable<firebase.User>;

  constructor(private db:AngularFireDatabase, 
              private authService:AuthService) { 
                
              }
 
  
}